//
//  WebVC.swift
//  TabBar
//
//  Created by Huort Seanghay on 12/9/22.
//

import UIKit
import WebKit

class WebVC: UIViewController, WKUIDelegate {

    @IBOutlet weak var webView: WKWebView!
    
    var urlStr : String?
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView?.uiDelegate = self
        view = webView
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let myURL = URL(string: self.urlStr ?? "") else { return }
        let myRequest = URLRequest(url: myURL)
        webView?.load(myRequest)
        
    }

}
