//
//  UserProfileVC.swift
//  TabBar
//
//  Created by Huort Seanghay on 12/9/22.
//

import UIKit
import WebKit

class UserProfileVC: UIViewController {

    @IBOutlet weak var backgroundView   : UIView!
    @IBOutlet weak var igButton         : UIButton!
    @IBOutlet weak var ytButton         : UIButton!
    @IBOutlet weak var lkButton         : UIButton!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundView.setBackground()
        backBtn?.tintColor = .white
    
    }
    
    @IBAction func didTapBackBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func didTapIGButton(_ sender: Any) {
        self.callWeb(url: "https://www.instagram.com/?hl=en")
    }
    @IBAction func didTapYTButton(_ sender: Any) {
        self.callWeb(url: "https://www.youtube.com/channel/UC-u9emd2ZWBnhaE2dgdkNMQ/videos")
    }
    @IBAction func didTapLKButton(_ sender: Any) {
        self.callWeb(url: "https://www.linkedin.com/in/seanghay-huort-745683214/")
    }
    
    // Func for push to webView
    func callWeb(url: String?) {
        let webKit = self.VC(sbName: "webSB", identifier: "WebVC") as! WebVC
        webKit.hidesBottomBarWhenPushed = true
        webKit.urlStr   = url ?? ""
        self.navigationController?.pushViewController(webKit, animated: true)
    }
    
    func VC(sbName: String, identifier: String) -> UIViewController {
        return UIStoryboard(name: sbName, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }

}
extension UserProfileVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "projectCell", for: indexPath)
            return cell
        }
    }
}

