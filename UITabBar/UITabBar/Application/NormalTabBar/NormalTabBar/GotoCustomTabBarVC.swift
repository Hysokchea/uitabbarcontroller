//
//  CustomTabBarVC.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 15/9/22.
//

import UIKit

class GotoCustomTabBarVC: UIViewController {

    @IBOutlet weak var tabBar       : UITabBarItem!
    @IBOutlet weak var gotoBtn      : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.repositionBadge(tabIndex: 0)
        self.gotoBtn.setTitle("Go to Next View", for: .normal)
        self.navigationItem.hidesBackButton = true
        self.tabBar?.badgeValue = "10"

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    @IBAction func didTapGoToAction(_ sender: UIButton) {
        self.pushViewController(sbName: "HomeSB", identifier: "NewViewVC")
        self.tabBarController?.tabBar.isHidden = true // Hidden Tab Bar when Push To other VC
    }
    
    func repositionBadge(tabIndex: Int){
        
        // Add Value to badge and set Color
        self.tabBar?.badgeValue = "Heesh"
        self.tabBar?.badgeColor = .darkGray

        //Change Badge Position
        for badgeView in self.tabBarController!.tabBar.subviews[tabIndex].subviews {

            if NSStringFromClass(badgeView.classForCoder) == "_UIBadgeView" {
                badgeView.layer.transform = CATransform3DIdentity
                badgeView.layer.transform = CATransform3DMakeTranslation(-70.0, 1.0, 1.0)
                
            }
        }
    }
}
