//
//  UIViewController.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 14/9/22.
//

import Foundation
import UIKit

extension UIViewController{
    func pushViewController(sbName: String, identifier: String, animated: Bool = true)  {
        let vc = UIStoryboard(name: sbName, bundle: nil).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(vc, animated: animated)
    }
}
